﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OwinSelfhostSample
{
    public class Program
    {
        static void Main()
        {
            string baseAddress = "http://localhost:9000/";

            // Start OWIN host 
            using (WebApp.Start<Startup>(url: baseAddress))
            {
                // Create HttpCient and make a request to api/values 
                HttpClient client = new HttpClient();

                var response = client.GetAsync(baseAddress + "api/values").Result;
                //var response = client.GetAsync(baseAddress + "api/values/1").Result;

                Console.WriteLine(response);
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Console.ReadLine();
            }

            // https://codeopinion.com/self-host-asp-net-web-api/
            //using (WebApp.Start<Startup>("http://localhost:9000"))
            //using (WebApp.Start<Startup>(url: baseAddress))
            //{
            //    Console.WriteLine("Web Server is running.");
            //    Console.WriteLine("Press any key to quit.");
            //    Console.ReadLine();
            //}
        }
    }
}
